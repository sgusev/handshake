/**************************************************************************/
// module       : handshake.v
// information  :
// author       : Sergey Gusev
// email        : GusevSergEvg@gmail.com
/**************************************************************************/

module handshake
(
 clk,
 rst,
//in
 i_WR         ,
 i_RD         ,
 i_RdyReq     ,
 i_Done       ,
 i_CODE_EMPTY ,
 i_CODE_ACK   ,
//out 
 o_ACK        ,
 o_Empty      ,
 o_CODE_WR    ,
 o_CODE_RD    ,
 o_CODE_RDYREQ
 );
    
    /**************************************************************************/
    input clk            ;
    input rst            ;
    input i_WR           ;
    input i_RD           ;
    input i_RdyReq       ;
    input i_Done         ;
    input i_CODE_EMPTY   ;
    input i_CODE_ACK     ;
    output o_ACK         ;        
    output reg o_Empty   ; 
    output o_CODE_WR     ; 
    output o_CODE_RD     ; 
    output o_CODE_RDYREQ ; 
    /**************************************************************************/

    reg   code_wr_s         = 1'b0;
    reg   code_rdyreq_wr_s  = 1'b0;
    reg   ack_wr_s          = 1'b0;
    reg   code_rd_s         = 1'b0;
    reg   code_rdyreq_rd_s  = 1'b0;
    reg   ack_rd_s          = 1'b0;

    reg   RdyReq_s_d1       = 1'b0;
    reg   RdyReq_s_d2       = 1'b0;
    reg   RdyReq_s_d3       = 1'b0;
    reg   RdyReq_s_d4       = 1'b0;

    reg   code_ack_s_d1     = 1'b0;

    //code_wr
    always @ (posedge clk) begin
        if (rst) begin
            code_wr_s   <= 1'b0;
            RdyReq_s_d2 <= 1'b0;
        end
        else begin
            RdyReq_s_d2 <= i_RdyReq;
            if (~i_WR || i_Done) begin
                code_wr_s <= 1'b0;
            end
            else if (i_WR && (i_RdyReq && ~RdyReq_s_d2)) begin
                code_wr_s <= 1'b1;
            end            
        end
    end
    //code_rdyreq_wr_s
    always @ (posedge clk) begin
        if (rst) begin
            code_rdyreq_wr_s    <= 1'b0;
            RdyReq_s_d1         <= 1'b0;
        end
        else begin
            RdyReq_s_d1 <= i_RdyReq;
            if (i_RdyReq && ~RdyReq_s_d1)
                code_rdyreq_wr_s <= 1'b1;          
            else if(i_CODE_ACK)
                code_rdyreq_wr_s <= 1'b0;  
        end
    end
    //ack_wr_s
    always @ (posedge clk) begin
        if (rst) begin
            ack_wr_s <= 1'b0;
        end
        else begin
            if(i_RdyReq && i_CODE_ACK)
                ack_wr_s <= 1'b1;
            else if (~i_RdyReq)
                ack_wr_s <= 1'b0;
        end
    end


    //code_rd,o_Empty
    always @ (posedge clk) begin
        if (rst) begin
            code_rd_s   <= 1'b0;
            RdyReq_s_d3 <= 1'b0;
            o_Empty     <= 1'b0;
        end
        else begin
            RdyReq_s_d3 <= i_RdyReq;
            if ((~i_RD || i_CODE_EMPTY) && (~i_RdyReq && ~ack_rd_s) && code_rd_s) begin
                code_rd_s <= 1'b0;
                if (i_CODE_EMPTY) begin
                    o_Empty <= 1'b1;
                end
            end
            else if(~i_RD)
                o_Empty <= 1'b0;
            else if ((i_RD && ~i_CODE_EMPTY) && (i_RdyReq && ~RdyReq_s_d3)) begin
                code_rd_s <= 1'b1;
            end            
        end
    end
    //code_rdyreq_rd_s
    always @ (posedge clk) begin
        if (rst) begin
            code_rdyreq_rd_s    <= 1'b0;
            RdyReq_s_d4         <= 1'b0;
        end
        else begin
            RdyReq_s_d4 <= i_RdyReq;
            if (i_RdyReq && ~RdyReq_s_d4)
                code_rdyreq_rd_s <= 1'b1;          
            else if(~i_RdyReq && RdyReq_s_d4)
                code_rdyreq_rd_s <= 1'b0;  
        end
    end
    //ack_rd_s
    always @ (posedge clk) begin
        if (rst) begin
            ack_rd_s <= 1'b0;
        end
        else begin
            code_ack_s_d1 <= i_CODE_ACK;

            if(~code_ack_s_d1 && i_CODE_ACK)
                ack_rd_s <= 1'b1;
            else if (~i_RdyReq)
                ack_rd_s <= 1'b0;
        end
    end

    assign o_CODE_WR            = code_wr_s;
    assign o_CODE_RDYREQ        = (code_rdyreq_wr_s & code_wr_s) | (code_rdyreq_rd_s & code_rd_s);
    assign o_ACK                = (ack_wr_s & code_wr_s) | (ack_rd_s & code_rd_s);

    assign o_CODE_RD            = code_rd_s;

endmodule // handshake
