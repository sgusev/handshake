vlib work

vlog +define+DEBUG_MODE ../src/handshake.v
vlog +define+DEBUG_MODE ./tb_handshake.sv

vsim -novopt -t 1ns tb_handshake

add wave -divider #MAIN#
add wave tb_handshake/uut/clk          
add wave tb_handshake/uut/rst                
add wave tb_handshake/uut/i_WR         
add wave tb_handshake/uut/i_RD         
add wave tb_handshake/uut/i_RdyReq     
add wave tb_handshake/uut/i_Done       
add wave tb_handshake/uut/i_CODE_EMPTY 
add wave tb_handshake/uut/i_CODE_ACK                
add wave tb_handshake/uut/o_ACK        
add wave tb_handshake/uut/o_Empty      
add wave tb_handshake/uut/o_CODE_WR    
add wave tb_handshake/uut/o_CODE_RD    
add wave tb_handshake/uut/o_CODE_RDYREQ

run 1ms