`timescale 1 ns / 1 ns
module tb_handshake;

    ///////////////////////////////////////////////////////////////////////////////
    localparam time pCLK_PERIOD = 20.8ns;
    ///////////////////////////////////////////////////////////////////////////////

    logic           clk               = 1'b0;         
    logic           rst               = 1'b0; 
    logic           i_WR              = 1'b0; 
    logic           i_RD              = 1'b0;         
    logic           i_RdyReq          = 1'b0; 
    logic           i_Done            = 1'b0; 
    logic           i_CODE_EMPTY      = 1'b0; 
    logic           i_CODE_ACK        = 1'b0;        
    logic           o_ACK             ; 
    logic           o_Empty           ; 
    logic           o_CODE_WR         ; 
    logic           o_CODE_RD         ; 
    logic           o_CODE_RDYREQ     ; 

    initial begin
        rst = 1'b1;
        #(1.3*pCLK_PERIOD);
        rst = 1'b0;
    end

    initial begin
        clk = 1'b0;
        forever
            #(pCLK_PERIOD/2) clk = ~clk;
    end

    initial begin
        @(negedge rst);
        #(5*pCLK_PERIOD);
        @(posedge clk);

        $display("Simulation started");
        i_WR <= 1'b1;        
        #(50*pCLK_PERIOD);
        @(negedge o_ACK);
        @(posedge clk);
        i_Done <= 1'b1;
        @(posedge clk);
        i_WR <= 1'b0;

        #(10*pCLK_PERIOD);

        i_RD <= 1'b1;
        #(50*pCLK_PERIOD);
        @(posedge i_CODE_ACK);
        @(posedge clk);
        i_CODE_EMPTY <= 1'b1;
        #(15*pCLK_PERIOD);
        i_RD         <= 1'b0;
    end

    // i_CODE_ACK
    initial begin
        forever begin
            @(posedge clk);
            while (o_CODE_RDYREQ != 1) begin
                @(posedge clk);
            end
            @(posedge clk);
            i_CODE_ACK <= 1'b1;
            while (o_CODE_RDYREQ != 0) begin
                @(posedge clk);
            end
            //@(posedge clk);
            i_CODE_ACK <= 1'b0;  
        end
    end

    //i_RdyReq
    initial begin
        forever begin
            @(posedge clk);
            @(posedge clk);
            while ((~i_RD && ~i_WR) || o_Empty) begin
                @(posedge clk);
            end
            #(2*pCLK_PERIOD);
        
            i_RdyReq <= 1'b1;
            while (o_ACK != 1) begin
                @(posedge clk);
            end    
            @(posedge clk);
            i_RdyReq <= 1'b0;
            @(posedge clk);
            @(posedge clk);
        end
    end    



    handshake
    uut
    (  
       .clk          (clk),
       .rst          (rst),
 
       .i_WR         (i_WR        ),
       .i_RD         (i_RD        ),
       .i_RdyReq     (i_RdyReq    ),
       .i_Done       (i_Done      ),
       .i_CODE_EMPTY (i_CODE_EMPTY),
       .i_CODE_ACK   (i_CODE_ACK  ),
       
       .o_ACK        (o_ACK        ),
       .o_Empty      (o_Empty      ),
       .o_CODE_WR    (o_CODE_WR    ),
       .o_CODE_RD    (o_CODE_RD    ),
       .o_CODE_RDYREQ(o_CODE_RDYREQ)
       );
    
endmodule
